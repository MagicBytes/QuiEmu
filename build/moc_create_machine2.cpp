/****************************************************************************
** Meta object code from reading C++ file 'create_machine2.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.8.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../src/create_machine2.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'create_machine2.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.8.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_create_machine2_t {
    QByteArrayData data[9];
    char stringdata0[149];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_create_machine2_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_create_machine2_t qt_meta_stringdata_create_machine2 = {
    {
QT_MOC_LITERAL(0, 0, 15), // "create_machine2"
QT_MOC_LITERAL(1, 16, 26), // "on_ram_slider_valueChanged"
QT_MOC_LITERAL(2, 43, 0), // ""
QT_MOC_LITERAL(3, 44, 5), // "value"
QT_MOC_LITERAL(4, 50, 23), // "on_previous_btn_clicked"
QT_MOC_LITERAL(5, 74, 21), // "on_cancel_btn_clicked"
QT_MOC_LITERAL(6, 96, 19), // "on_next_btn_clicked"
QT_MOC_LITERAL(7, 116, 27), // "on_ram_size_txt_textChanged"
QT_MOC_LITERAL(8, 144, 4) // "arg1"

    },
    "create_machine2\0on_ram_slider_valueChanged\0"
    "\0value\0on_previous_btn_clicked\0"
    "on_cancel_btn_clicked\0on_next_btn_clicked\0"
    "on_ram_size_txt_textChanged\0arg1"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_create_machine2[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       5,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    1,   39,    2, 0x08 /* Private */,
       4,    0,   42,    2, 0x08 /* Private */,
       5,    0,   43,    2, 0x08 /* Private */,
       6,    0,   44,    2, 0x08 /* Private */,
       7,    1,   45,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void, QMetaType::Int,    3,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,    8,

       0        // eod
};

void create_machine2::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        create_machine2 *_t = static_cast<create_machine2 *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->on_ram_slider_valueChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 1: _t->on_previous_btn_clicked(); break;
        case 2: _t->on_cancel_btn_clicked(); break;
        case 3: _t->on_next_btn_clicked(); break;
        case 4: _t->on_ram_size_txt_textChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObject create_machine2::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_create_machine2.data,
      qt_meta_data_create_machine2,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *create_machine2::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *create_machine2::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_create_machine2.stringdata0))
        return static_cast<void*>(const_cast< create_machine2*>(this));
    return QWidget::qt_metacast(_clname);
}

int create_machine2::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 5)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 5;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 5)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 5;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
