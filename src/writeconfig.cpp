#include <unistd.h>
#include <sys/types.h>
#include <pwd.h>
#include "src/writeconfig.h"

void writeConfig::write2Config(QString configFile, QString MachineName, QString SystemType, QString CD_DestinationFileName ,QString DiskImage, int ramSize, QString MachineType) {
    // Create a new file
    QFile file(configFile);
    file.open(QIODevice::WriteOnly | QIODevice::Text);
    QTextStream out(&file);
    out << "{\n\"MachineName\": \"" + MachineName + "\",\n";
      out << "\"SystemType\": \"" + SystemType + "\",\n";
      out << "\"MachineType\": \"" + MachineType + "\",\n";
      out << "\"CDImage\": \"" + CD_DestinationFileName + "\",\n";
      out << "\"DiskImage\": \"" + DiskImage + "\"" + "," + "\n";
      out << "\"RamSize\": \"" + QString::number(ramSize) + "\"" + "\n";
    out << '}';

    // optional, as QFile destructor will already do it:
    file.close();
}

void writeConfig::write2Config(QString configFile, QString MachineName, QString SystemType, QString CD_DestinationFileName, QString DiskImage, int ramSize, QString MachineType, bool enable_kvm, QString CPU_Type, bool enable_spice, int spice_port, bool enable_graphics, QString GraphicsCard, bool enable_networking, QString NetworkInterface1, QString NetworkInterface2, QString NetworkInterface3, bool bypass_host_interface) {
    // Create a new file
    QFile file(configFile);
    file.open(QIODevice::WriteOnly | QIODevice::Text);
    QTextStream out(&file);
    out << "{\n\"MachineName\": \"" + MachineName + "\",\n";
      out << "\"SystemType\": \"" + SystemType + "\",\n";
      out << "\"MachineType\": \"" + MachineType + "\",\n";
      out << "\"CPUType\": \"" + CPU_Type + "\",\n";
      out << "\"CDImage\": \"" + CD_DestinationFileName + "\",\n";
      out << "\"DiskImage\": \"" + DiskImage + "\"" + "," + "\n";
      out << "\"RamSize\": \"" + QString::number(ramSize) + "\"" + "," + "\n";
      out << "\"EnableKVM\": " + QString(enable_kvm ? "true" : "false") + "," + "\n";
      out << "\"EnableSPICE\": " + QString(enable_spice ? "true" : "false") + "," + "\n";
      out << "\"SpicePort\": \"" + QString::number(spice_port) + "\"" + "," + "\n";
      out << "\"EnableGraphics\": " + QString(enable_graphics ? "true" : "false") + "," + "\n";
      out << "\"GraphicsCard\": \"" + GraphicsCard + "\"" + "," + "\n";
      out << "\"EnableNetworking\": " + QString(enable_networking ? "true" : "false") + "," + "\n";
      out << "\"NetworkInterface1\": \"" + NetworkInterface1 + "\"" + "," + "\n";
      out << "\"NetworkInterface2\": \"" + NetworkInterface2 + "\"" + "," + "\n";
      out << "\"NetworkInterface3\": \"" + NetworkInterface3 + "\"" + "," + "\n";
      out << "\"BypassHostInterface\": " + QString(bypass_host_interface ? "true" : "false") + "\n";
    out << '}';

    // optional, as QFile destructor will already do it:
    file.close();
}
