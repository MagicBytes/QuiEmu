/********************************************************************************
** Form generated from reading UI file 'create_machine2.ui'
**
** Created by: Qt User Interface Compiler version 5.8.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_CREATE_MACHINE2_H
#define UI_CREATE_MACHINE2_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSlider>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_create_machine2
{
public:
    QLineEdit *ram_size_txt;
    QPushButton *cancel_btn;
    QLabel *label_2;
    QPushButton *previous_btn;
    QLabel *label;
    QSlider *ram_slider;
    QPushButton *next_btn;

    void setupUi(QWidget *create_machine2)
    {
        if (create_machine2->objectName().isEmpty())
            create_machine2->setObjectName(QStringLiteral("create_machine2"));
        create_machine2->resize(928, 542);
        ram_size_txt = new QLineEdit(create_machine2);
        ram_size_txt->setObjectName(QStringLiteral("ram_size_txt"));
        ram_size_txt->setGeometry(QRect(600, 230, 113, 22));
        QFont font;
        font.setFamily(QStringLiteral("Open Sans"));
        ram_size_txt->setFont(font);
        cancel_btn = new QPushButton(create_machine2);
        cancel_btn->setObjectName(QStringLiteral("cancel_btn"));
        cancel_btn->setGeometry(QRect(300, 350, 81, 22));
        label_2 = new QLabel(create_machine2);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setGeometry(QRect(730, 230, 21, 16));
        previous_btn = new QPushButton(create_machine2);
        previous_btn->setObjectName(QStringLiteral("previous_btn"));
        previous_btn->setGeometry(QRect(410, 350, 81, 22));
        label = new QLabel(create_machine2);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(410, 120, 91, 31));
        QFont font1;
        font1.setFamily(QStringLiteral("Open Sans"));
        font1.setPointSize(14);
        label->setFont(font1);
        ram_slider = new QSlider(create_machine2);
        ram_slider->setObjectName(QStringLiteral("ram_slider"));
        ram_slider->setGeometry(QRect(160, 230, 421, 16));
        ram_slider->setMaximum(64000);
        ram_slider->setSingleStep(100);
        ram_slider->setPageStep(20);
        ram_slider->setOrientation(Qt::Horizontal);
        ram_slider->setInvertedAppearance(false);
        ram_slider->setTickPosition(QSlider::TicksBelow);
        ram_slider->setTickInterval(2000);
        next_btn = new QPushButton(create_machine2);
        next_btn->setObjectName(QStringLiteral("next_btn"));
        next_btn->setGeometry(QRect(520, 350, 81, 22));

        retranslateUi(create_machine2);

        QMetaObject::connectSlotsByName(create_machine2);
    } // setupUi

    void retranslateUi(QWidget *create_machine2)
    {
        create_machine2->setWindowTitle(QApplication::translate("create_machine2", "Form", Q_NULLPTR));
        cancel_btn->setText(QApplication::translate("create_machine2", "Cancel", Q_NULLPTR));
        label_2->setText(QApplication::translate("create_machine2", "MB", Q_NULLPTR));
        previous_btn->setText(QApplication::translate("create_machine2", "Previous", Q_NULLPTR));
        label->setText(QApplication::translate("create_machine2", "RAM Size", Q_NULLPTR));
        next_btn->setText(QApplication::translate("create_machine2", "Next", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class create_machine2: public Ui_create_machine2 {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_CREATE_MACHINE2_H
