# QuiEmu
QuiEmu is a graphical QEMU frontend written in C++ with Qt5

Development is still Work in Progress

<ul>
<li> Diskimage Creator: Done. :heavy_check_mark: </li>
<li> Machine Creator: Done. :heavy_check_mark: </li>
<li> qt json parser backend: Done :heavy_check_mark: </li>
<li> Machine Lister: Done :heavy_check_mark: </li>
<li> Machine Remover: Done :heavy_check_mark: </li>
<li> CD-Image Chooser: Done :heavy_check_mark: </li>
<li> CPU-Selector: Done :heavy_check_mark: </li>
<li> RAM-Selector: Done :heavy_check_mark: </li>
<li> Acceleration Manager: Done. :heavy_check_mark: </li>
<li> Spice Support: Done. :heavy_check_mark: </li>
<li> Machine Details Lister: Done. :heavy_check_mark: </li>
<li> Graphics Manager: Done. :heavy_check_mark: </li>
<li> Theme Feature: Done. :heavy_check_mark: </li>
<li> Networking Manager: Partly done. </li>
<li> Machine Runner: Almost Done </li>
<li> Machine Modifier: Almost Done </li>
</ul>
<br><br>

Building is done through the build directory

## Screenshots
<img src="https://i.imgur.com/fbjUzyr.png">
<br>
<img src="https://i.imgur.com/wL41xOd.png">

