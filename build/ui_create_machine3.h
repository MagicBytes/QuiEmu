/********************************************************************************
** Form generated from reading UI file 'create_machine3.ui'
**
** Created by: Qt User Interface Compiler version 5.8.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_CREATE_MACHINE3_H
#define UI_CREATE_MACHINE3_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QRadioButton>
#include <QtWidgets/QToolButton>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_create_machine3
{
public:
    QLabel *label;
    QGroupBox *groupBox;
    QRadioButton *radio0;
    QRadioButton *radio1;
    QToolButton *file_choose_btn;
    QLineEdit *lineEdit;
    QPushButton *finish_next_btn;
    QPushButton *previous_btn;

    void setupUi(QWidget *create_machine3)
    {
        if (create_machine3->objectName().isEmpty())
            create_machine3->setObjectName(QStringLiteral("create_machine3"));
        create_machine3->resize(615, 381);
        label = new QLabel(create_machine3);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(100, 30, 501, 41));
        QFont font;
        font.setFamily(QStringLiteral("Open Sans"));
        font.setPointSize(14);
        label->setFont(font);
        groupBox = new QGroupBox(create_machine3);
        groupBox->setObjectName(QStringLiteral("groupBox"));
        groupBox->setGeometry(QRect(100, 80, 411, 251));
        radio0 = new QRadioButton(groupBox);
        radio0->setObjectName(QStringLiteral("radio0"));
        radio0->setGeometry(QRect(90, 70, 171, 20));
        radio0->setChecked(true);
        radio1 = new QRadioButton(groupBox);
        radio1->setObjectName(QStringLiteral("radio1"));
        radio1->setGeometry(QRect(90, 120, 161, 20));
        file_choose_btn = new QToolButton(groupBox);
        file_choose_btn->setObjectName(QStringLiteral("file_choose_btn"));
        file_choose_btn->setEnabled(false);
        file_choose_btn->setGeometry(QRect(320, 160, 28, 22));
        lineEdit = new QLineEdit(groupBox);
        lineEdit->setObjectName(QStringLiteral("lineEdit"));
        lineEdit->setEnabled(false);
        lineEdit->setGeometry(QRect(90, 160, 231, 22));
        finish_next_btn = new QPushButton(groupBox);
        finish_next_btn->setObjectName(QStringLiteral("finish_next_btn"));
        finish_next_btn->setGeometry(QRect(310, 210, 81, 22));
        previous_btn = new QPushButton(groupBox);
        previous_btn->setObjectName(QStringLiteral("previous_btn"));
        previous_btn->setGeometry(QRect(30, 210, 81, 22));

        retranslateUi(create_machine3);

        QMetaObject::connectSlotsByName(create_machine3);
    } // setupUi

    void retranslateUi(QWidget *create_machine3)
    {
        create_machine3->setWindowTitle(QApplication::translate("create_machine3", "Form", Q_NULLPTR));
        label->setText(QApplication::translate("create_machine3", "Create new disk image or use an existing one", Q_NULLPTR));
        groupBox->setTitle(QString());
        radio0->setText(QApplication::translate("create_machine3", "Create new Disk Image", Q_NULLPTR));
        radio1->setText(QApplication::translate("create_machine3", "Use an existing one", Q_NULLPTR));
        file_choose_btn->setText(QApplication::translate("create_machine3", "...", Q_NULLPTR));
        finish_next_btn->setText(QApplication::translate("create_machine3", "Next", Q_NULLPTR));
        previous_btn->setText(QApplication::translate("create_machine3", "Previous", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class create_machine3: public Ui_create_machine3 {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_CREATE_MACHINE3_H
