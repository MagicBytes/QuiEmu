#ifndef CREATE_MACHINE_H
#define CREATE_MACHINE_H

#include <QApplication>
#include <QWidget>
#include <QtGui>
#include <QString>
#include <QErrorMessage>
#include "create_machine2.h"
#include "create_machine3.h"

namespace Ui {
class create_machine;
}

class create_machine : public QWidget {
    Q_OBJECT

public:
    Ui::create_machine *ui;
    explicit create_machine(QWidget *parent = 0);
    ~create_machine();
    create_machine2 *myCreate_machine2Window;

private slots:
    void on_next_btn_clicked();

    void on_cancel_btn_clicked();
};

#endif // CREATE_MACHINE_H
