/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.8.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QLocale>
#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QListView>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTreeView>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QAction *actionDiskimage;
    QAction *actionMachine;
    QAction *actionAbout_QuiEmu;
    QAction *actionAppearance;
    QWidget *centralwidget;
    QGroupBox *groupBox;
    QWidget *verticalLayoutWidget;
    QVBoxLayout *verticalLayout;
    QListView *machine_listView;
    QGroupBox *groupBox_2;
    QWidget *verticalLayoutWidget_2;
    QVBoxLayout *verticalLayout_2;
    QTreeView *machine_details_treeView;
    QWidget *horizontalLayoutWidget;
    QHBoxLayout *horizontalLayout;
    QPushButton *run_machine_btn;
    QPushButton *modify_machine_btn;
    QPushButton *remove_machine_btn;
    QMenuBar *menubar;
    QMenu *menuNew;
    QMenu *menuSettings;
    QMenu *menuAbout;
    QStatusBar *statusbar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->setWindowModality(Qt::NonModal);
        MainWindow->resize(802, 597);
        MainWindow->setLocale(QLocale(QLocale::English, QLocale::Germany));
        actionDiskimage = new QAction(MainWindow);
        actionDiskimage->setObjectName(QStringLiteral("actionDiskimage"));
        actionMachine = new QAction(MainWindow);
        actionMachine->setObjectName(QStringLiteral("actionMachine"));
        actionAbout_QuiEmu = new QAction(MainWindow);
        actionAbout_QuiEmu->setObjectName(QStringLiteral("actionAbout_QuiEmu"));
        actionAppearance = new QAction(MainWindow);
        actionAppearance->setObjectName(QStringLiteral("actionAppearance"));
        centralwidget = new QWidget(MainWindow);
        centralwidget->setObjectName(QStringLiteral("centralwidget"));
        groupBox = new QGroupBox(centralwidget);
        groupBox->setObjectName(QStringLiteral("groupBox"));
        groupBox->setGeometry(QRect(0, 0, 251, 551));
        groupBox->setAlignment(Qt::AlignCenter);
        groupBox->setFlat(false);
        verticalLayoutWidget = new QWidget(groupBox);
        verticalLayoutWidget->setObjectName(QStringLiteral("verticalLayoutWidget"));
        verticalLayoutWidget->setGeometry(QRect(0, 20, 251, 531));
        verticalLayout = new QVBoxLayout(verticalLayoutWidget);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        machine_listView = new QListView(verticalLayoutWidget);
        machine_listView->setObjectName(QStringLiteral("machine_listView"));
        machine_listView->setEditTriggers(QAbstractItemView::NoEditTriggers);

        verticalLayout->addWidget(machine_listView);

        groupBox_2 = new QGroupBox(centralwidget);
        groupBox_2->setObjectName(QStringLiteral("groupBox_2"));
        groupBox_2->setGeometry(QRect(250, 0, 551, 551));
        groupBox_2->setAlignment(Qt::AlignCenter);
        verticalLayoutWidget_2 = new QWidget(groupBox_2);
        verticalLayoutWidget_2->setObjectName(QStringLiteral("verticalLayoutWidget_2"));
        verticalLayoutWidget_2->setGeometry(QRect(0, 20, 551, 491));
        verticalLayout_2 = new QVBoxLayout(verticalLayoutWidget_2);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        verticalLayout_2->setContentsMargins(0, 0, 0, 0);
        machine_details_treeView = new QTreeView(verticalLayoutWidget_2);
        machine_details_treeView->setObjectName(QStringLiteral("machine_details_treeView"));
        machine_details_treeView->setEditTriggers(QAbstractItemView::NoEditTriggers);
        machine_details_treeView->setAnimated(true);
        machine_details_treeView->setExpandsOnDoubleClick(false);

        verticalLayout_2->addWidget(machine_details_treeView);

        horizontalLayoutWidget = new QWidget(groupBox_2);
        horizontalLayoutWidget->setObjectName(QStringLiteral("horizontalLayoutWidget"));
        horizontalLayoutWidget->setGeometry(QRect(0, 510, 551, 41));
        horizontalLayout = new QHBoxLayout(horizontalLayoutWidget);
        horizontalLayout->setSpacing(6);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        horizontalLayout->setContentsMargins(6, 0, 6, 0);
        run_machine_btn = new QPushButton(horizontalLayoutWidget);
        run_machine_btn->setObjectName(QStringLiteral("run_machine_btn"));
        run_machine_btn->setAutoDefault(false);
        run_machine_btn->setFlat(false);

        horizontalLayout->addWidget(run_machine_btn);

        modify_machine_btn = new QPushButton(horizontalLayoutWidget);
        modify_machine_btn->setObjectName(QStringLiteral("modify_machine_btn"));

        horizontalLayout->addWidget(modify_machine_btn);

        remove_machine_btn = new QPushButton(horizontalLayoutWidget);
        remove_machine_btn->setObjectName(QStringLiteral("remove_machine_btn"));

        horizontalLayout->addWidget(remove_machine_btn);

        MainWindow->setCentralWidget(centralwidget);
        menubar = new QMenuBar(MainWindow);
        menubar->setObjectName(QStringLiteral("menubar"));
        menubar->setGeometry(QRect(0, 0, 802, 23));
        menuNew = new QMenu(menubar);
        menuNew->setObjectName(QStringLiteral("menuNew"));
        menuSettings = new QMenu(menubar);
        menuSettings->setObjectName(QStringLiteral("menuSettings"));
        menuAbout = new QMenu(menubar);
        menuAbout->setObjectName(QStringLiteral("menuAbout"));
        MainWindow->setMenuBar(menubar);
        statusbar = new QStatusBar(MainWindow);
        statusbar->setObjectName(QStringLiteral("statusbar"));
        MainWindow->setStatusBar(statusbar);

        menubar->addAction(menuNew->menuAction());
        menubar->addAction(menuSettings->menuAction());
        menubar->addAction(menuAbout->menuAction());
        menuNew->addAction(actionDiskimage);
        menuNew->addAction(actionMachine);
        menuSettings->addAction(actionAppearance);
        menuAbout->addAction(actionAbout_QuiEmu);

        retranslateUi(MainWindow);

        run_machine_btn->setDefault(false);


        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", Q_NULLPTR));
        actionDiskimage->setText(QApplication::translate("MainWindow", "Disk Image", Q_NULLPTR));
        actionMachine->setText(QApplication::translate("MainWindow", "Machine", Q_NULLPTR));
        actionAbout_QuiEmu->setText(QApplication::translate("MainWindow", "About QuiEmu", Q_NULLPTR));
        actionAppearance->setText(QApplication::translate("MainWindow", "Appearance", Q_NULLPTR));
        groupBox->setTitle(QApplication::translate("MainWindow", "Machines", Q_NULLPTR));
        groupBox_2->setTitle(QApplication::translate("MainWindow", "Machine Details", Q_NULLPTR));
        run_machine_btn->setText(QApplication::translate("MainWindow", "Run", Q_NULLPTR));
        modify_machine_btn->setText(QApplication::translate("MainWindow", "Modify", Q_NULLPTR));
        remove_machine_btn->setText(QApplication::translate("MainWindow", "Remove", Q_NULLPTR));
        menuNew->setTitle(QApplication::translate("MainWindow", "New", Q_NULLPTR));
        menuSettings->setTitle(QApplication::translate("MainWindow", "Settings", Q_NULLPTR));
        menuAbout->setTitle(QApplication::translate("MainWindow", "About", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
