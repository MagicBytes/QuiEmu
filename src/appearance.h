#ifndef APPEARANCE_H
#define APPEARANCE_H

#include <QApplication>
#include <QProcess>
#include <QWidget>
#include <QFile>
#include <unistd.h>
#include <sys/types.h>
#include <pwd.h>
#include <QJsonDocument>
#include <QJsonObject>
#include <QTextStream>

namespace Ui {
class appearance;
}

class appearance : public QWidget
{
    Q_OBJECT

public:
    explicit appearance(QWidget *parent = 0);
    ~appearance();

private slots:
    void on_apply_btn_clicked();

    void on_cancel_btn_clicked();

private:
    Ui::appearance *ui;
    QString theme;
    struct passwd *pw = getpwuid(getuid());
    const char *homedir = pw->pw_dir;
    QString val;
    QFile file;
};

#endif // APPEARANCE_H
