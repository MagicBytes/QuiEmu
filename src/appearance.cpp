#include "src/appearance.h"
#include "ui_appearance.h"

appearance::appearance(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::appearance) {
        ui->setupUi(this);
        file.setFileName(QString::fromUtf8(homedir) + "/.config/QuiEmu/appearance.json");
        file.open(QIODevice::ReadOnly | QIODevice::Text);
        val = file.readAll();
        file.close();
        QJsonDocument AppearanceFile = QJsonDocument::fromJson(val.toUtf8());
        QJsonObject AppearanceHandler = AppearanceFile.object();
        QJsonValue Theme = AppearanceHandler.value(QString("Theme"));

        if(Theme.toString()=="dark") {
            ui->theme_comboBox->setCurrentIndex(1);
        } else {
            ui->theme_comboBox->setCurrentIndex(0);
        }
}

appearance::~appearance() {
    delete ui;
}

void appearance::on_apply_btn_clicked() {
    switch (ui->theme_comboBox->currentIndex()) {
        case 0:
            theme = "light";
        break;
        case 1:
            theme = "dark";
        break;
    }
    QString appearanceFile(QString::fromUtf8(homedir) + "/.config/QuiEmu/appearance.json");

    QFile file(appearanceFile);
    file.open(QIODevice::WriteOnly | QIODevice::Text);
    QTextStream out(&file);
    out << "{\n\"Theme\": \"" + theme + "\"" + "\n";
    out << '}';

    // optional, as QFile destructor will already do it:
    file.close();
    this->close();
    qApp->quit();
    QProcess::startDetached(qApp->arguments()[0], qApp->arguments());

}

void appearance::on_cancel_btn_clicked() {
    this->close();
    qApp->quit();
    QProcess::startDetached(qApp->arguments()[0], qApp->arguments());
}
