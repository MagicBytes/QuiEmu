/****************************************************************************
** Meta object code from reading C++ file 'modify_machine.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.8.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../src/modify_machine.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'modify_machine.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.8.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_modify_machine_t {
    QByteArrayData data[21];
    char stringdata0[534];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_modify_machine_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_modify_machine_t qt_meta_stringdata_modify_machine = {
    {
QT_MOC_LITERAL(0, 0, 14), // "modify_machine"
QT_MOC_LITERAL(1, 15, 20), // "on_apply_btn_clicked"
QT_MOC_LITERAL(2, 36, 0), // ""
QT_MOC_LITERAL(3, 37, 26), // "on_ram_slider_valueChanged"
QT_MOC_LITERAL(4, 64, 5), // "value"
QT_MOC_LITERAL(5, 70, 27), // "on_ram_size_txt_textChanged"
QT_MOC_LITERAL(6, 98, 8), // "ramSize2"
QT_MOC_LITERAL(7, 107, 37), // "on_create_new_diskimage_radio..."
QT_MOC_LITERAL(8, 145, 35), // "on_use_existing_image_radio_c..."
QT_MOC_LITERAL(9, 181, 27), // "on_image_choose_btn_clicked"
QT_MOC_LITERAL(10, 209, 27), // "on_choose_cdrom_btn_clicked"
QT_MOC_LITERAL(11, 237, 29), // "on_enable_cd_checkbox_toggled"
QT_MOC_LITERAL(12, 267, 7), // "checked"
QT_MOC_LITERAL(13, 275, 35), // "on_enable_graphics_checkbox_t..."
QT_MOC_LITERAL(14, 311, 32), // "on_enable_spice_checkbox_toggled"
QT_MOC_LITERAL(15, 344, 19), // "on_exit_btn_clicked"
QT_MOC_LITERAL(16, 364, 37), // "on_enable_Networking_checkBox..."
QT_MOC_LITERAL(17, 402, 30), // "on_enable_if1_checkBox_toggled"
QT_MOC_LITERAL(18, 433, 30), // "on_enable_if2_checkBox_toggled"
QT_MOC_LITERAL(19, 464, 30), // "on_enable_if3_checkBox_toggled"
QT_MOC_LITERAL(20, 495, 38) // "on_enable_samba_share_checkBo..."

    },
    "modify_machine\0on_apply_btn_clicked\0"
    "\0on_ram_slider_valueChanged\0value\0"
    "on_ram_size_txt_textChanged\0ramSize2\0"
    "on_create_new_diskimage_radio_clicked\0"
    "on_use_existing_image_radio_clicked\0"
    "on_image_choose_btn_clicked\0"
    "on_choose_cdrom_btn_clicked\0"
    "on_enable_cd_checkbox_toggled\0checked\0"
    "on_enable_graphics_checkbox_toggled\0"
    "on_enable_spice_checkbox_toggled\0"
    "on_exit_btn_clicked\0"
    "on_enable_Networking_checkBox_toggled\0"
    "on_enable_if1_checkBox_toggled\0"
    "on_enable_if2_checkBox_toggled\0"
    "on_enable_if3_checkBox_toggled\0"
    "on_enable_samba_share_checkBox_toggled"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_modify_machine[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      16,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,   94,    2, 0x08 /* Private */,
       3,    1,   95,    2, 0x08 /* Private */,
       5,    1,   98,    2, 0x08 /* Private */,
       7,    0,  101,    2, 0x08 /* Private */,
       8,    0,  102,    2, 0x08 /* Private */,
       9,    0,  103,    2, 0x08 /* Private */,
      10,    0,  104,    2, 0x08 /* Private */,
      11,    1,  105,    2, 0x08 /* Private */,
      13,    1,  108,    2, 0x08 /* Private */,
      14,    1,  111,    2, 0x08 /* Private */,
      15,    0,  114,    2, 0x08 /* Private */,
      16,    1,  115,    2, 0x08 /* Private */,
      17,    1,  118,    2, 0x08 /* Private */,
      18,    1,  121,    2, 0x08 /* Private */,
      19,    1,  124,    2, 0x08 /* Private */,
      20,    1,  127,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,    4,
    QMetaType::Void, QMetaType::QString,    6,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Bool,   12,
    QMetaType::Void, QMetaType::Bool,   12,
    QMetaType::Void, QMetaType::Bool,   12,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Bool,   12,
    QMetaType::Void, QMetaType::Bool,   12,
    QMetaType::Void, QMetaType::Bool,   12,
    QMetaType::Void, QMetaType::Bool,   12,
    QMetaType::Void, QMetaType::Bool,   12,

       0        // eod
};

void modify_machine::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        modify_machine *_t = static_cast<modify_machine *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->on_apply_btn_clicked(); break;
        case 1: _t->on_ram_slider_valueChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 2: _t->on_ram_size_txt_textChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 3: _t->on_create_new_diskimage_radio_clicked(); break;
        case 4: _t->on_use_existing_image_radio_clicked(); break;
        case 5: _t->on_image_choose_btn_clicked(); break;
        case 6: _t->on_choose_cdrom_btn_clicked(); break;
        case 7: _t->on_enable_cd_checkbox_toggled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 8: _t->on_enable_graphics_checkbox_toggled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 9: _t->on_enable_spice_checkbox_toggled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 10: _t->on_exit_btn_clicked(); break;
        case 11: _t->on_enable_Networking_checkBox_toggled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 12: _t->on_enable_if1_checkBox_toggled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 13: _t->on_enable_if2_checkBox_toggled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 14: _t->on_enable_if3_checkBox_toggled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 15: _t->on_enable_samba_share_checkBox_toggled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObject modify_machine::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_modify_machine.data,
      qt_meta_data_modify_machine,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *modify_machine::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *modify_machine::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_modify_machine.stringdata0))
        return static_cast<void*>(const_cast< modify_machine*>(this));
    return QWidget::qt_metacast(_clname);
}

int modify_machine::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 16)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 16;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 16)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 16;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
