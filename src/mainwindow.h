#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QApplication>
#include <QMainWindow>
#include <QMessageBox>
#include <QThread>
#include <QListView>
#include <QFile>
#include <QJsonDocument>
#include <QJsonObject>
#include <QStringList>
#include <unistd.h>
#include <sys/types.h>
#include <pwd.h>
#include "create_diskimage.h"
#include "create_machine.h"
#include "modify_machine.h"
#include "parseconfig.h"
#include "appearance.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow {
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    Ui::MainWindow *ui;
    QStringListModel *model;
    QStringList machines;
    QListView *machineView;
    struct passwd *pw = getpwuid(getuid());
    const char *homedir = pw->pw_dir;
    void loadMachines();
    void listMachineDetails(int index);
    MainWindow *myMainWindow;
    create_machine2 *myCreate_machine2Window;

private slots:
    void on_actionAbout_QuiEmu_triggered();

    void on_actionDiskimage_triggered();

    void on_actionMachine_triggered();

    void on_run_machine_btn_clicked();

    void on_modify_machine_btn_clicked();

    void on_remove_machine_btn_clicked();

    void on_machine_listView_clicked();

    void on_actionAppearance_triggered();

private:
    create_diskimage *myCreate_diskimageWindow;
    create_machine *myCreate_machineWindow;
    modify_machine *myModify_machineWindow;
    appearance *myAppearanceWindow;
    static void ListMachineFunction();
    void MachineContextMenu();
    void ParseConfig(QString configFile);

};

#endif // MAINWINDOW_H
