/********************************************************************************
** Form generated from reading UI file 'choose_cd_image.ui'
**
** Created by: Qt User Interface Compiler version 5.8.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_CHOOSE_CD_IMAGE_H
#define UI_CHOOSE_CD_IMAGE_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QRadioButton>
#include <QtWidgets/QToolButton>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_choose_cd_image
{
public:
    QLabel *label;
    QLineEdit *cd_image_path_txt;
    QToolButton *choose_cdrom_btn;
    QWidget *horizontalLayoutWidget;
    QHBoxLayout *horizontalLayout;
    QPushButton *cancel_btn;
    QPushButton *previous_btn;
    QPushButton *next_btn;
    QRadioButton *yes_radio_btn;
    QRadioButton *no_radio_btn;

    void setupUi(QWidget *choose_cd_image)
    {
        if (choose_cd_image->objectName().isEmpty())
            choose_cd_image->setObjectName(QStringLiteral("choose_cd_image"));
        choose_cd_image->resize(765, 464);
        label = new QLabel(choose_cd_image);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(270, 70, 211, 51));
        QFont font;
        font.setPointSize(12);
        label->setFont(font);
        cd_image_path_txt = new QLineEdit(choose_cd_image);
        cd_image_path_txt->setObjectName(QStringLiteral("cd_image_path_txt"));
        cd_image_path_txt->setGeometry(QRect(110, 190, 471, 22));
        choose_cdrom_btn = new QToolButton(choose_cd_image);
        choose_cdrom_btn->setObjectName(QStringLiteral("choose_cdrom_btn"));
        choose_cdrom_btn->setGeometry(QRect(580, 190, 31, 21));
        horizontalLayoutWidget = new QWidget(choose_cd_image);
        horizontalLayoutWidget->setObjectName(QStringLiteral("horizontalLayoutWidget"));
        horizontalLayoutWidget->setGeometry(QRect(150, 320, 431, 41));
        horizontalLayout = new QHBoxLayout(horizontalLayoutWidget);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        horizontalLayout->setContentsMargins(0, 0, 0, 0);
        cancel_btn = new QPushButton(horizontalLayoutWidget);
        cancel_btn->setObjectName(QStringLiteral("cancel_btn"));

        horizontalLayout->addWidget(cancel_btn);

        previous_btn = new QPushButton(horizontalLayoutWidget);
        previous_btn->setObjectName(QStringLiteral("previous_btn"));

        horizontalLayout->addWidget(previous_btn);

        next_btn = new QPushButton(horizontalLayoutWidget);
        next_btn->setObjectName(QStringLiteral("next_btn"));

        horizontalLayout->addWidget(next_btn);

        yes_radio_btn = new QRadioButton(choose_cd_image);
        yes_radio_btn->setObjectName(QStringLiteral("yes_radio_btn"));
        yes_radio_btn->setGeometry(QRect(340, 140, 100, 20));
        yes_radio_btn->setChecked(true);
        no_radio_btn = new QRadioButton(choose_cd_image);
        no_radio_btn->setObjectName(QStringLiteral("no_radio_btn"));
        no_radio_btn->setGeometry(QRect(340, 250, 100, 20));

        retranslateUi(choose_cd_image);

        QMetaObject::connectSlotsByName(choose_cd_image);
    } // setupUi

    void retranslateUi(QWidget *choose_cd_image)
    {
        choose_cd_image->setWindowTitle(QApplication::translate("choose_cd_image", "Form", Q_NULLPTR));
        label->setText(QApplication::translate("choose_cd_image", "Choose a CD-ROM Image", Q_NULLPTR));
        choose_cdrom_btn->setText(QApplication::translate("choose_cd_image", "...", Q_NULLPTR));
        cancel_btn->setText(QApplication::translate("choose_cd_image", "Cancel", Q_NULLPTR));
        previous_btn->setText(QApplication::translate("choose_cd_image", "Previous", Q_NULLPTR));
        next_btn->setText(QApplication::translate("choose_cd_image", "Next", Q_NULLPTR));
        yes_radio_btn->setText(QApplication::translate("choose_cd_image", "Yes", Q_NULLPTR));
        no_radio_btn->setText(QApplication::translate("choose_cd_image", "No Thanks", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class choose_cd_image: public Ui_choose_cd_image {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_CHOOSE_CD_IMAGE_H
