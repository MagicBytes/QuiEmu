#-------------------------------------------------
#
# Project created by QtCreator 2017-03-17T21:37:31
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = quiemu
TEMPLATE = app


SOURCES += src/main.cpp \
    src/mainwindow.cpp \
    src/create_diskimage.cpp \
    src/create_machine.cpp \
    src/create_machine2.cpp \
    src/create_machine3.cpp \
    src/writeconfig.cpp \
    src/parseconfig.cpp \
    src/run_machine.cpp \
    src/choose_cd_image.cpp \
    src/modify_machine.cpp \
    src/appearance.cpp

HEADERS  += \
    src/mainwindow.h \
    src/create_diskimage.h \
    src/create_machine.h \
    src/create_machine2.h \
    src/create_machine3.h \
    src/writeconfig.h \
    src/parseconfig.h \
    src/run_machine.h \
    src/choose_cd_image.h \
    src/modify_machine.h \
    src/appearance.h

FORMS    += \
    src/forms/mainwindow.ui \
    src/forms/create_diskimage.ui \
    src/forms/create_machine.ui \
    src/forms/create_machine2.ui \
    src/forms/create_machine3.ui \
    src/forms/choose_cd_image.ui \
    src/forms/modify_machine.ui \
    src/forms/appearance.ui
